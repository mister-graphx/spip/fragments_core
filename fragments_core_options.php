<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Ajout des raccourcis dans la liste des wheels
$GLOBALS['spip_wheels']['raccourcis'][] = 'fragments_core.yaml';
$GLOBALS['spip_wheels']['raccourcis'][] = 'box.yaml';
$GLOBALS['spip_wheels']['raccourcis'][] = 'colonnes.yaml';

// Définir la google analitic uid
// @deprecated
// if(!defined('_GA_UID')) {
// 	define('_GA_UID', false);
// }
