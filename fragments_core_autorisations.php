<?php
/**
 * Fragments : Core
 * (c) 2016 (Mist. GraphX)
 * Licence MIT
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

// declaration vide pour ce pipeline.
function fragments_core_autoriser(){}

/**
 * autoriser_pays_menu
 *
 * masque l'affichage du menu edition>pays
 *
*/
function autoriser_pays_menu($faire, $type, $id, $qui, $opt){
  // en attendant de trouver mieux ou de comprendre pourquoi
  // return autoriser('webmestre', $type, $id, $qui, $opt);
  // ne renvoie rien
  return false;
}



/**
 * autoriser_travaux()
 *
 * autoriser les contributeurs a voir le site pendant la maintenance
 *
 * @see - http://contrib.spip.net/En-travaux-2-0
*/
function autoriser_travaux($faire,$quoi,$id,$qui,$opts){
	if ($qui['statut']=='0minirezo' OR $qui['statut']=='1comite')
		return true;
	return false;
}

?>
