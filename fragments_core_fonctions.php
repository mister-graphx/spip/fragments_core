<?php
/**
 * Fragments - Core
 * (c) 2016-2019 (Mist. GraphX)
 * Licence MIT
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

// Fonctions et balise utiles pour la Configuration
// du skelette
// balise SETTING
// Chargement des yaml de config
include_spip('inc/fragments_config');

/**
 * Generer les boutons d'admin des forum selon les droits du visiteur
 * en SPIP >= 2.1 uniquement
 *
 * tiré du plugin comments
 *
 * @param object $p
 * @return object
 */
function balise_BOUTONS_ADMIN_FORUM($p) {
	if (($_id = interprete_argument_balise(1,$p))===NULL)
		$_id = champ_sql('id_forum', $p);

		$p->code = "
'<'.'?php
	if (isset(\$GLOBALS[\'visiteur_session\'][\'statut\'])
	  AND \$GLOBALS[\'visiteur_session\'][\'statut\']==\'0minirezo\'
		AND (\$id = '.intval($_id).')
		AND	include_spip(\'inc/autoriser\')
		AND autoriser(\'moderer\',\'forum\',\$id)) {
			include_spip(\'inc/actions\');include_spip(\'inc/filtres\');
			echo \"<div class=\'boutons spip-admin actions modererforum\'>\"
			. bouton_action(_T(\'fragments_core:button_comment_delete\'),generer_action_auteur(\'instituer_forum\',\$id.\'-off\',ancre_url(self(),\'forum\')),\'poubelle\')
			. bouton_action(_T(\'fragments_core:button_comment_spam\'),generer_action_auteur(\'instituer_forum\',\$id.\'-spam\',ancre_url(self(),\'forum\')),\'spam\')
			. \"</div>\";
		}
?'.'>'";

	$p->interdire_scripts = false;
	return $p;
}

include_spip('inc/charsets');
include_spip('inc/texte');
include_spip('inc/plugin'); // pour plugin_est_installe
include_spip('inc/xml');

/**
 * export_distrib()
 *
 * retourne a la pipeline mes_fichiers_a_sauver,
 * la liste des modules (plugins,extensions) a exporter
 *
 * @param $type {string} plugins, extensions
 * @return $datas {array} liste du type de modules passé en paramètre (plugins, extensions)
*/
function export_distrib($flux,$type){
    //liste_prefix_plugin_actifs est la liste des prefixes des plugins actifs
    $liste_prefix_plugin_actifs = liste_chemin_plugin_actifs();
    // $liste_prefix_extensions_actives est la liste des prefixes des extensions actives
    $liste_prefix_extensions_actives = liste_plugin_files(_DIR_PLUGINS_DIST);
    switch ($type) {
        case 'plugins':
            $datas['plugins'] = fragments_core_afficher_list(self(), $liste_prefix_plugin_actifs,$liste_plugins_actifs, _DIR_PLUGINS, $format='liste',$afficheQuoi,$params);
        break;
        case 'extensions':
            $datas['extensions'] = fragments_core_afficher_list(self(), $liste_prefix_extensions_actives,$liste_extensions_actives, _DIR_PLUGINS_DIST, $format='liste',$afficheQuoi,$params);
        break;
        default:
            $datas['plugins'] = fragments_core_afficher_list(self(), $liste_prefix_plugin_actifs,$liste_plugins_actifs, _DIR_PLUGINS, $format='liste',$afficheQuoi,$params);
            $datas['extensions'] = fragments_core_afficher_list(self(), $liste_prefix_extensions_actives,$liste_extensions_actives, _DIR_PLUGINS_DIST, $format='liste',$afficheQuoi,$params);
    }

    return $datas;
}

/**
 * fragments_core_afficher_list()
 *
 * retourne les informations détaillées des modules (plugins, extensions)
 *
 * @see http://code.spip.net/autodoc/tree/ecrire/plugins/afficher_liste.php.html#function_plugins_afficher_liste_dist
 *
*/
function fragments_core_afficher_list($url_page,$liste_plugins, $liste_plugins_actifs, $dir_plugins,$afficheQuoi){
	$get_infos = charger_fonction('get_infos','plugins');
  $distrib=array();
	foreach($liste_plugins as $prefix => $chemin) {
			$info = $get_infos($chemin, false, $dir_plugins);
      $repos_url = get_repos(realpath(_DIR_PLUGINS.$chemin));
			$distrib[$prefix]['nom'] = $info['nom'];
			$distrib[$prefix]['prefix'] = $info['prefix'];
			$distrib[$prefix]['version'] = $info['version'];
      $distrib[$prefix]['path'] = $chemin;
      $distrib[$prefix]['repository'] = $repos_url;
	}
  return $distrib;
}


/*
 * function get_repos
 * @param $working_copy
 * @return array() - repository url
 */
function get_repos($working_copy) {
    exec('svn info '.$working_copy, $svn_infos);
    $svn_infos = preg_replace('#URL: #i','',$svn_infos[2],1);
    return $svn_infos;
}

/**
 * array2table
 *
 *
 *
 * Une fonction récursive pour joliment afficher #ENV, #GET, #SESSION...
 *    en squelette : [(#ENV|array2table)], [(#GET|array2table)], [(#SESSION|array2table)]
 *    ou encore [(#ARRAY{0,1, a,#SESSION, 1,#ARRAY{x,y}}|array2table)]
 *
 * @see Tiré du plugin dev ou encore des astuces longues du carnet contrib
 * @param string|array $env
 *    si une string est passée elle doit être le serialize d'un array
 *
 * @return string
 *    une chaîne html affichant une <table>
**/
function array2table($env) {
    $env = str_replace(array('&quot;', '&#039;'), array('"', '\''), $env);
    if (is_array($env_tab = @unserialize($env))) {
        $env = $env_tab;
    }
    if (!is_array($env)) {
        return '';
    }
    $style = " style='border:1px solid #ddd;'";
    $res = "<table style='border-collapse:collapse;'>\n";
    foreach ($env as $nom => $val) {
        if (is_array($val) || is_array(@unserialize($val))) {
            $val = array2table($val);
        }
        else {
            $val = entites_html($val);
        }
        $res .= "<tr>\n<td$style><strong>". entites_html($nom).
                   "&nbsp;:&nbsp;</strong></td><td$style>" .$val. "</td>\n</tr>\n";
    }
    $res .= "</table>";
    return $res;
}



/**
 * menu2table
 *
 * transforme un menu ul >li en table >tr > td
 *
 * @param int $nbcols
 *    nombre de colonnes par lignes
 * @param string $css
 *    class pour le tableau
 * @param string $attributes
 *    attributs supplémentaires (ex colspan, …)
 *
 * @return string
 *    une chaîne html affichant une <table>
**/
function filtre_menu2table_dist($flux, $nbcols = 4, $css=null, $attributes=null){

	$xml = new SimpleXMLElement($flux);
	$output = array();
	$i = 0;

	foreach($xml->li as $li){
		$output[$i]['link'] = (string) $li->a[0];
		$output[$i]['href'] = (string) $li->a['href'];
		$i++;
	}

	$nbcels = count($output);

	$nbrows = ($nbcels / $nbcols);
	$totalcels = ceil($nbrows) * $nbcols ;
	(isset($css)) ? $css = ' class="'.$css.'"': $css = '' ;
	(isset($attributes)) ? $css .= ' '.$attributes : $attributes = '';

	$table = '<table'.$css.'>';
	$i= 1;
	foreach($output as $cel){
		($i == 1 ) ? $table .= "<tr>" : '' ;

		$td = '<td><a href="'.$cel['href'].'" >'.$cel['link'].'</a></td>'."\n";
		$table .= $td;
		($i % $nbcols == 0 && !$i = $nbcels) ? $table .= "</tr>\n<tr>" : '' ;

		if($i == $nbcels) {
				//ajouter des td vides pour completer la lignes
				for($rest = 1 ; $rest <= ($totalcels - $nbcels); $rest++ ){
					$table .= '<td></td>';
				}
				$table .= '</tr></table>'."\n";
		}

		$i++;
	}

	return $table;
}



/**
 * Balise VIGNETTE
 *
 * etat: dev
 *
 * @todo - a reecrire avec objet/id_objet
 * @see - http://romy.tetue.net/logos-automatiques-articles-SPIP
 * @see - Modele logo objet : https://contrib.spip.net/Astuces-longues-pour-SPIP#a15
*/
function balise_VIGNETTE_dist($p) {
	$_id_article = champ_sql('id_article', $p);
	$_id_rubrique= champ_sql('id_rubrique', $p);

	$p->code = "recuperer_fond('modeles/vignette',array('id_article'=>$_id_article,'id_rubrique'=>$_id_rubrique))";
	$p->interdire_scripts = false;
	return $p;
}


/*
 * function balise_INFO_LOGO_dist
 *
 * @example
 * 	[(#INFO_LOGO**{#OBJET,#ID_OBJET})]
 * @param $objet
 * @param $id_objet
 * @param $mode "on" ou "off" suivant le logo normal ou survol
 *
 */
function balise_INFO_LOGO_dist($p) {
	$id_objet = interprete_argument_balise(2,$p);
   $objet = interprete_argument_balise(1,$p);
	if(!$mode = interprete_argument_balise(3,$p))
		$mode ='on';

	$fichier = ($p->etoile === '**') ? 1 : 0;

	$p->code = "trouver_logo_objet($objet,$id_objet,$mode,$fichier)";
	$p->interdire_scripts = false;

	return $p;
}

function trouver_logo_objet($objet,$id_objet,$mode='on', $fichier=false){
	include_spip('public/quete');
	include_spip('inc/filtres');

	if($fichier)
		return quete_logo($objet,$mode,$id_objet, 1, 1);

	$logo = quete_logo_objet($id_objet, $objet,$mode);

	return http_img_pack($logo['chemin'],'');
}

/*
 * function balise_SETTINGS_dist
 *
 * @example
 * 	[(#SETTINGS{emails/})]
 * @param $objet
 * @param $id_objet
 * @param $mode "on" ou "off" suivant le logo normal ou survol
 *
 */
function balise_THEME_SETTINGS_dist($p){

	$req = interprete_argument_balise(1,$p);

	$p->code = "init_theme()";
	$p->interdire_scripts = false;

	return $p;
}



function init_theme(){
	return $theme_settings = array(
		'base' => [
			'base-font-size'=> '1rem',
			'base-font-family'=> 'sans-serif'
		],
		'colors' => [
			'primary'=>'',
			'secondary'=>'',
		],
		'emails' => [
			'base-background' => '#FFFFFF'
		]
	);
}


/*
 * function balise_GA_TRACKING
 *
 * usage:
 * 	#GA_TRACKING{gtag}
 * si la global _GA_UID n'est pas définie c'est la config qui est utilisée
 *
 * @param $mode gtag/analitic si pas de parametre analitic est par défault
 */
function balise_GA_TRACKING_dist($p) {
	$mode = interprete_argument_balise(1,$p);
	$p->code = "genere_analitics($mode)";
	$p->interdire_scripts = false;
	return $p;
}

function genere_analitics($mode = 'analitic'){
	$config= lire_config('fragments');
	if($guid = $config['ga_tracking']){
		if($mode == 'gtag'){
			return "<script async src=\"https://www.googletagmanager.com/gtag/js?id=".$guid."\"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', '".$guid."');
</script>
";
		} else {
			return "<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', '".$guid."', 'auto');
ga('send', 'pageview');
</script>";

		}

	}
}


/* filtre nofolow

Etat : test/dev

Ajout de  nofollow sur les liens (pas mal à utiliser  sur les commentaires pour éviter le spam)

@see https://contrib.spip.net/Balise-rel-nofollow-pour-les-liens


*/
function nofollow($texte){
   $texte = str_replace('<a href','<a rel="nofollow" href',$texte);
   return $texte;
}

/**
 * fragments_lister_saisies()
 * Lister les saisies disponibles dans les dossiers saisies/
 *
 * Etat : dev
 *
 * @staticvar array $liste_saisies
 * @return array
 */
function fragments_lister_saisies(){
	static $liste_saisies = false;

	if ($liste_saisies === false) {
		$liste_saisies = array();
		$match = "[^-]*[.]html$";
		$liste = find_all_in_path('saisies/', $match);

		if (count($liste)){
			foreach($liste as $saisie => $chemin){
				$liste_saisies[$saisie]['path'] = $chemin ;
				$content= file_get_contents($chemin);
				$liste_saisies[$saisie]['content'] = extraire_doc_blocks($content);
				$liste_saisies[$saisie]['params'] = extraire_params($liste_saisies[$saisie]['content']);
			}
		}
	}
	return $liste_saisies;
}


/*
 * function extraire_doc_blocks
 *
 * on tente de limiter a la première occurence trouvée
 *
 * http://lumadis.be/regex/test_regex.php?id=2866
 * @param $flux
 */
function extraire_doc_blocks($flux) {
	$regex = '#(\[\(\#REM\))((.)+)(\])#Uims';
	$doc_block = preg_match_all($regex, $flux, $match, PREG_OFFSET_CAPTURE, 0);
	$doc_block = str_replace(array('<!--','-->'),'',$match[2][0]);
	return $doc_block[0];
}

/*
 * function extraire_params
 * @param $flux
 */

function extraire_params($flux) {
    $match = preg_match('/Parametres\s:/',$flux,$match);
	//var_dump($match);

}

?>
