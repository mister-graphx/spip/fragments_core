# CHANGELOGS

1.2.14

* boutons administration masquer le bouton ajouté par skeleditor, le mode inclure est dispo via le menu debug

* Changement de terme dans le titre de config

1.2.13

* correctif sur la largeur de la barre d'outils administration
* passer a system-ui pour la barre d'outils administration
* propager la font (system-ui) dans les liens des sous-menus
* pas de schema dans le paquet xml evite des signalement d'erreur a l'installation

1.2.12

suppression des chaînes de langues inutiles, reportées dans ieconfigplus

1.2.11

* surcharge la liste des plugins de svp (prive/squelette/inclure) afin d'afficher les liens documentations et développement des plugins, ajout des chaines de langue dans un fichier svp_fr.
* ajout d'un fichier CHANGELOGS.
