<?php

if (!defined('_ECRIRE_INC_VERSION')) {
  return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
  'bulle_aller_developpement' => "Se rendre sur l'espace de développement",
  'lien_developpement' => 'Développement (tikets, historique)'
);
