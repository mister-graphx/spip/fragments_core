<?php

if (!defined('_ECRIRE_INC_VERSION')) {
  return;
}

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/manuel_site/lang/

// necessite : manuelsite :http://contrib.spip.net/Manuel-de-redaction-du-site

// Ce fichier regroupes FAQS et questions fréquentes
// les chaine se compose d'une question (sufix _q), la chaine suivant étant la réponsse
// l'affichages des blocs faqs est similaire aux chaines de langues ou modèles :
// exemple = <faq|p=jangoofaqs|b=raccourcis_redacteurs>
// p = prefixe du plugin
// b = bloc de faqs
// q=non -> pour ne pas afficher la réponse
// suivis de paramètres si besoin |p1=param1|p2=param2
// Paramètres : exemple
//  {{@rubrique@}}
$GLOBALS[$GLOBALS['idx_lang']] = array(
  'typo_raccourcis_q'=> 'Racourcis typopgraphiques',
  'typo_raccourcis'=>'
-* Centrer un paragraphe : <code><center>Texte centré</center></code>
-* Eviter les débordement de texte avec <code><clear></code>
'
);
