<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// F
	'fragments_core_titre'	=> 'Fragments Core',

	// B
	'bouton_afficher_forum'	=> 'Voir Admin forum',
	'bouton_masquer_forum'	=> 'Masquer Admin forum',

	// I
	'button_comment_delete'	=> '<span class="icon icon-trash" aria-hidden="true"></span>Supprimer',
	'button_comment_spam'	=> '<span class="icon icon-warning" aria-hidden="true"></span>Signaler',

	// T

);
