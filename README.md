FRAGMENTS Core
--------------

Surcharges et ajouts au CMS SPIP.
Activations des plugins les plus couramment utilisés sur tout les projets.


## Esthétique UX

* Coté public amélioration de la barre d'outils d'administration (masquage, affichage, menu debug …)

* surcharge la liste des plugins pour afficher les liens ouvrants (`_blank`) vers dev et doc quand renseigné dans le paquet.xml du plugin.

* Ajoute une page Plugins actifs, listant tout les plugins actif, sous forme de necessite, avec leur numéro de version, pour faire un copier coller dans un `paquet.xml`, par exemple.


## Pages de configuration extensible

Chaque fichier `fragments_config_xx.yaml`
situé a la racine du path de spip est chargé dans le panneau de configuration `?exec=configurer_fragments`.

Les configurations sont décrites par une liste d'index principaux que l'on peut organiser par catégories
SEO, Newsletter, Modes, Techniques, Theme, …
Cet index est la clef du tableau, il est décrit par un titre une explication et une liste d'éléments.

Un élément doit posséder une uid unique afin de ne pas être écrasé par d'autres configs.
Ensuite on passe les paramètres qui produirons la saisie


```yaml
seo:
  titre: 'SEO'
  explication: 'Optimisation du référencement'
  elements:
      -
        uid: 'ga_tracking'
        label: 'Google Analitics UID'
        explication: 'Votre identifiant Google Analitics'
        saisie: 'input'
        defaut: ''
```


## Balises

### SETTING

La balise SETTING permet de retourner plus facilement une valeur de configuration stockée dans le casier des meta : fragments.

Si la valeur de configuration n'est pas trouvé elle ne retourne rien.

```html
#SETTING{ga_tracking}
```


### GA_TRACKING

`#GA_TRACKING` qui inssère les codes de suivi,la google-uid est a définir dans la configuration du squelette.

Par défaut la balise insère le code analitics,
on peut cependant passer en parametre 'gtag', pour utiliser l'autre version
du code de suivi utilisant tagManager.


## Spip Cli

`spipmu monsite.local "fragments:plugins:lister --archiver --no-dist"`

- exporte les plugins actifs du site dans un dossier `/tmp/export_plugins`.
