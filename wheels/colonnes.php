<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// @see http://zone.spip.org/trac/spip-zone/browser/_plugins_/todo/trunk/wheels/todo.php

// La wheel renvoie un tableau à cette callback qui est le résultat d'un preg_match_all.
// Le contenu du tableau est le suivant :
// 0 - la chaine complète
// 1 - ouverture de la boite
// 2 - ouverture de la boite
// 3 - Contenu
// 4 - balise fermante

function row($t){
	return "<div class='row$t[2]'>$t[3]</div>" ;
}

function col($t){
	return "<div class='col$t[2]'>$t[3]</div>";
}
