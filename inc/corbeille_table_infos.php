<?php
/**
 * Fragments - Core
 * (c) 2016 (Mist. GraphX)
 * Licence MIT
 *
 * Pipelines
 * @see http://programmer.spip.net/-Liste-des-pipelines-
 *
 *
 */
if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * pipeline fragments_core_corbeille_table_infos()
 *
 * Plugin Corbeille
 *
 * pour prendre en compte plugieur statuts :
 * - creer une noisette par statut /liste/objet_statut permettant l'affichage du statut de l'objet
 * 	 nomée corbeille_objet_statut
 *
 * - ajouter via la pipeline corbeille_table_infos les informations de la tache de suppression
 * 	 l'index etant le nom du fichier sans le prefixe corbeille_
 * 		'objet_statut'
 * 			'statut': string - un seul statut
 * 			'table': string - table dans le cas de plusieurs status, l'index servant a definir la noisette
 * 			'tableliee': array - les tables a traiter avec le prefixe spip_table_lien
*/
function fragments_core_corbeille_table_infos($params){

    if(test_plugin_actif('mailsubscribers')){
        $params['mailsubscribers'] = array (
    			"statut" => "poubelle",
    			"table" => "mailsubscribers",
        );

        $params['mailsubscribers_refuse'] = array(
      			"statut" => "refuse",
      			"table" => "mailsubscribers",
      	);
    }

	if(test_plugin_actif('mailshot')){
        $params['mailshots'] = array(
      			"statut" => "poubelle",
      			"table"=>"mailshots",
      			"tableliee" => array("spip_mailshots","spip_mailshots_destinataires"),
        );

        $params['mailshots_cancel'] = array(
      			"statut" => "cancel",
      			"table"=>"mailshots",
      			"tableliee" => array("spip_mailshots","spip_mailshots_destinataires"),
        );
	}

  if(test_plugin_actif('newsletters')){
      $params['newsletters'] = array(
    		"statut" => "poubelle",
    		"table" => "newsletters",
    		"tableliee"=> array("spip_documents","spip_newletters_liens","spip_documents_liens"),
      );
  }



    $params['documents'] = array(
        "statut" => "poubelle",
        "tableliee"=> array("spip_documents_liens"),
    );

    $params['forums'] = array(
        "statut" => "poubelle",
		      "table"=>'forum'
    );

    return $params;
}

?>
