<?php
/**
 * Fragments - Core
 * (c) 2018 (Mist. GraphX)
 * Licence MIT
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/* Lister les config ajoutées via les fichiers de config

Chaque module/skel peut ajouter sa config via un fichier yaml
placé dans a la racine du repertoire du plugin
le fichier doit commencer par fragments_config et suivi par le prefix du plugin
si le nom de fichier est identique find_all_in_path s'arrete au premier trouvé

*/
function lister_fragments_config(){
	include_spip('inc/yaml');
	$config = array();
	if($files = find_all_in_path('', '/fragments_config')){
		foreach($files as $file){
			if(is_array($config) && yaml_decode_file($file))
				$config = array_merge_recursive($config,yaml_decode_file($file));
		}
		ksort($config);
	}
	return $config;
}


/* balise SETTINGS

Retourne une configuration stocké
dans les metas de fragments
sinon un tableau de toute les configs
*/
function balise_SETTING_dist($p){

	$req = interprete_argument_balise(1,$p);

	$p->code = "get_fragments_setting($req)";
	$p->interdire_scripts = false;

	return $p;
}

function get_fragments_setting($setting){
		include_spip('inc/config');
		if($setting = lire_config('fragments/'.$setting) )
			return $setting;
		else {
			// return array2table(lire_config('fragments'));
			return false;
		}
}
