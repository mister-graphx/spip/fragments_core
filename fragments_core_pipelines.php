<?php
/**
 * Fragments - Core
 * (c) 2016-2019 (Mist. GraphX)
 * Licence MIT
 *
 * Pipelines
 * @see http://programmer.spip.net/-Liste-des-pipelines-
 *
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

include_spip('inc/utils'); // test_plugin_actif


function fragments_core_ieconfig_metas($table){
	$table['fragments']['titre'] = 'fragments';
	$table['fragments']['icone'] = 'prive/themes/spip/images/fragments_core-16.png';
	$table['fragments']['metas_serialize'] = 'fragments';
	return $table;
}

/**
 * fragments_core_header_prive()
 *
 * permet d’ajouter des contenus dans la partie <head> des pages de l’espace privé.
 *
 * @see - http://programmer.spip.net/header_prive
 *
 *
*/
function fragments_core_header_prive($flux) {
		// Styles perso previsu /admin_preview.{css,scss}
		// Inssérer une feuille de style pour la prévisu du porte-plume
		// prend en charge un fichier scss si scssphp est dispo
		if(include_spip('scssphp_fonctions') and function_exists('scss_select_css')){
			$typeset = scss_select_css("css/admin_preview.css") ;
		}else {
			$typeset = find_in_path("admin_preview.css");
		}

		$flux .= '<link rel="stylesheet" href="' . $typeset  .'" type="text/css" media="all" />';
    return $flux;
}

/**
 * fragments_core_mes_fichiers_a_sauver()
 *
 * rajouter des fichiers a sauvegarder dans le plugin Mes Fichiers 2
 *
 * @param $flux
 * @return array
 */
function fragments_core_mes_fichiers_a_sauver($flux){

  $plugins_actif = export_distrib($flux,'plugins');

  foreach($plugins_actif['plugins'] as $plugin){
      if(@is_dir(_DIR_PLUGINS.$plugin['path']))
          $flux[] = _DIR_PLUGINS.$plugin['path'];
  }

	spip_log("[CORE] Export de la distribution du site",'fragments');
	spip_log($flux,'fragments');

	return $flux;
}
